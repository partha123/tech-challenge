﻿using System.Data.Entity.Core.Objects;
using AutoComplete.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutoComplete.Controllers
{
    public class TitleClass
    {
        public int TitleId { get; set; }
        public string Name { get; set; }
    }
    public class HomeController : Controller
    {
        private readonly TitlesEntities dbContext;

        public HomeController()
        {
            this.dbContext = new TitlesEntities();
        }

        /// <summary>
        /// To show the list of title list
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(int? id)
        {
            try
            {
                var titles = new List<Title>();
                if (id != null)
                {
                    titles = this.dbContext.Titles.Include("TitleParticipants")
                        .Where(x => x.TitleId == id).ToList();
                    return View(titles);
                }
                return View(titles);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// To fetch titles as per the matching serach pattern
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public JsonResult GetTitles(string query)
        {
            try
            {
                var q = (from t in this.dbContext.Titles
                         where t.TitleName.Contains(query)
                         select new TitleClass
                         {
                             TitleId = t.TitleId,
                             Name = t.TitleName
                         }).ToList().OrderByDescending(x => x.Name);


                return Json(q, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// To fetch the selected title detail by using title id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PartialViewResult GetTitleById(int id)
        {
            try
            {
                var title = this.dbContext.Titles.FirstOrDefault(e => e.TitleId == id);
                return PartialView("../Shared/TitleResult", title);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public ActionResult GetTitleDetailById(int id)
        //{
        //    try
        //    {
        //        var title = this.dbContext.Titles.FirstOrDefault(e => e.TitleId == id);
        //        return View(title);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
    }
}